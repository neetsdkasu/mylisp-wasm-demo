# THIRDPARTY

SUMMARY  

 - このプロジェクトmylisp-wasm-demoは[wasm-pack](https://github.com/rustwasm/wasm-pack)を使用しました(`new`や`build`で生成されたファイルをこのリポジトリに含んでいます)
 - このプロジェクトmylisp-wasm-demoの`Cargo.toml`に依存関係のCrateが書いてあります
 - Webデモページで[CodeMirror](https://github.com/codemirror/CodeMirror)を使用してます(`site/lib/codemirror.*`,`site/addon/hint/show-hint.*`)
