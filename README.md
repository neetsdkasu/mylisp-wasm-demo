# mylisp-wasm-demo

[mylisp](https://bitbucket.org/neetsdkasu/mylisp)の一部をWebAssemblyを使ってデモを作った


```bash
$ rustup toolchain list
stable-i686-pc-windows-msvc (default)

$ cargo --version
cargo 1.56.0 (4ed5d137b 2021-10-04)

$ rustc --version
rustc 1.56.1 (59eed8a2a 2021-11-01)

$ wasm-pack --version
wasm-pack 0.10.1

$ cargo-generate --version
cargo-generate 0.11.0

$ wasm-bindgen --version
wasm-bindgen 0.2.78
```