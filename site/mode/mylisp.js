// CodeMirror mode mylisp
// author: Leonardone @ NEETSDKASU

CodeMirror.defineMode("mylisp", function(conf, parserConf) {
    "use strict";

    const specialform = [
        "cond", "define", "lamgda", "quote",
        "quasiquote", "unquote",
        "unquote-splicing", "macro",
        "if", "recur", "next",
        "delay", "delay-get"
    ];

    const builtin = [
        "atom", "car", "cdr", "cons", "eq",
        "+", "-", "*", "div", "/", "rem",
        "=", "<", "<=", ">", ">=",
        "bool", "and", "or", "not",
        "time-now",
        "str-concat", "str-substr",
        "str-split", "str-to-num",
        "num-to-str", "str-compare", "str-length",
        "cast-int32", "cast-int64", "cast-float64",
        "bitwise-and", "bitwise-or",
        "bitwise-xor", "bitwise-not",
        "shift-left", "shift-right",
        "num-to-bin-str",
        "num-to-hex-str",
        "nil?", "list?", "list", "equals",
        "println", "readln", "eprintln", "error"
    ];

    const atom = [
        "true", "false", "nil"
    ];

    CodeMirror.registerHelper("hintWords", "mylisp",
        specialform.concat(builtin).concat(atom)
    );

    function takeString(stream) {
        do {
            stream.match(/^[^\\"]+/);
        } while (stream.match(/^\\./));
        return stream.match('"');
    }

    function token(stream, state) {
        if (state.in_str) {
            state.in_str = !takeString(stream);
            return "string";
        }
        if (stream.eatSpace()) {
            return null;
        }
        if (stream.match(";")) {
            stream.skipToEnd();
            return "comment";
        }
        if (stream.match('"')) {
            state.in_str = !takeString(stream);
            return "string";
        }
        if (stream.match(/^[\(\)]/)) {
            return "bracket";
        }
        if (stream.match(/^['`,\.]/)) {
            return "operator";
        }
        if (stream.match(/^-?\d[A-FX_\d\.\-\+]*/i)) {
            // 雑すぎる正規表現パース…
            // 整数 1_234_567
            // 16進数 0x1234ABCD
            // 小数 123.456e+10
            return "number";
        }
        if (stream.match(/^[^\s\(\)'`,\.";]+/i)) {
            const word = stream.current().toLowerCase();
            if (specialform.indexOf(word) >= 0) {
                return "keyword";
            }
            if (builtin.indexOf(word) >= 0) {
                return "builtin";
            }
            if (atom.indexOf(word) >= 0) {
                return "atom";
            }
            return null;
        }
        stream.skipToEnd();
        return "error";
    }

    const mode = {
        startState: function() { return { in_str: false }; },
        token: token,
    };

    return mode;
});

CodeMirror.defineMIME("text/x-neetsdkasu-mylisp", "mylisp");

