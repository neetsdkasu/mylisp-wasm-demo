import init_mylisp, { MyLispEngine } from "./mylisp_wasm_demo.js";

const result = document.getElementById("result");
const stdout = document.getElementById("stdout");
const stderr = document.getElementById("stderr");
const runButton = document.getElementById("run");
const srcTextArea = CodeMirror.fromTextArea(
    document.getElementById("src"),
    { lineNumbers: true, mode: "mylisp", readOnly: true,
        extraKeys: {"Ctrl-Space": "autocomplete"} }
);

let engine = null;

window.appendToStdout = function(s) {
    stdout.textContent = stdout.textContent + s;
};

window.appendToStderr = function(s) {
    stderr.textContent = stderr.textContent + s;
}

function lockElements(locked) {
    runButton.disabled = locked;
    srcTextArea.setOption("readOnly", locked ? "nocursor" : false);
}

function clear() {
    result.textContent = "";
    stdout.textContent = "";
    stderr.textContent = "";
}

function clickRun() {
    lockElements(true);
    clear();
    const src = srcTextArea.getValue();
    engine.setSource(src);
    if (window.requestAnimationFrame) {
        window.requestAnimationFrame(runEngine);
    } else {
        setTimeout(runEngine, 1);
    }
}

function runEngine() {
    const res = engine.run();
    if (res.ok) {
        const code = res.take_result();
        if (typeof code === "string") {
            result.textContent = code;
            if (window.requestAnimationFrame) {
                window.requestAnimationFrame(runEngine);
            } else {
                setTimeout(runEngine, 1);
            }
            return;
        }
    }
    lockElements(false);
};

init_mylisp().then(() => {
    engine = new MyLispEngine();
    runButton.addEventListener("click", clickRun);
    lockElements(false);
});