mod utils;

use mylisp_code::{Case, Code, InjectFuncInfo, Syntax};
use mylisp_engine::{Engine, IOSet};
use mylisp_parser::Parser;
use mylisp_tokenizer::Tokenizer;
use std::io;
use std::rc::Rc;
use wasm_bindgen::prelude::*;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
extern "C" {
    type Date;

    #[wasm_bindgen(static_method_of = Date)]
    pub fn now() -> f64;

    fn prompt() -> JsValue;

    #[wasm_bindgen(js_name = appendToStdout)]
    fn append_to_stdout(s: &str);
    #[wasm_bindgen(js_name = appendToStderr)]
    fn append_to_stderr(s: &str);
}

#[derive(Default)]
struct Prompt {
    buf: Vec<u8>,
    offset: usize,
}

struct Output {
    f: fn(&str),
}

#[wasm_bindgen]
pub struct MyLispResult {
    pub ok: bool,
    result: Option<String>,
}

#[wasm_bindgen]
pub struct MyLispEngine {
    engine: Engine<Prompt, io::LineWriter<Output>, io::LineWriter<Output>>,
    parser: Parser<io::Cursor<String>>,
}

fn time_now(_: &[Rc<Code>]) -> Result<Rc<Code>, String> {
    Ok(Code::Int64(Box::new(Date::now().floor() as i64)).rc())
}

fn time_now_code() -> Rc<Code> {
    Code::InjectFunc(Rc::new(InjectFuncInfo {
        id: 0,
        name: Rc::new("time-now".to_string()),
        arg_min_size: 0,
        arg_max_size: 0,
        call_back: time_now,
    }))
    .rc()
}

#[wasm_bindgen]
impl MyLispEngine {
    #[wasm_bindgen(constructor)]
    pub fn new() -> Self {
        utils::set_panic_hook();
        let io_set = IOSet {
            input: Prompt::default(),
            output: io::LineWriter::new(Output {
                f: append_to_stdout,
            }),
            error: io::LineWriter::new(Output {
                f: append_to_stderr,
            }),
            case: Case::Lower,
            syntax: Syntax::Shorten,
        };
        let mut engine = Engine::new(io_set);
        let code = time_now_code();
        engine.bind_special_code("time-now", code).unwrap();
        let tokenizer = Tokenizer::new(io::Cursor::new("".to_string()));
        let parser = Parser::new(tokenizer);
        Self { engine, parser }
    }

    #[wasm_bindgen(js_name = setSource)]
    pub fn set_source(&mut self, src: String) {
        let tokenizer = Tokenizer::new(io::Cursor::new(src));
        let parser = Parser::new(tokenizer);
        self.parser = parser;
    }

    pub fn run(&mut self) -> MyLispResult {
        use std::io::Write;

        let mut ok = false;
        let mut result: Option<Rc<Code>> = None;

        match self.parser.parse() {
            Err(err) => {
                writeln!(&mut self.engine.io_set.error, "{}", err).unwrap();
            }
            Ok(parse_result) => match parse_result {
                Ok(None) => {
                    ok = true;
                }
                Err(err) => {
                    writeln!(&mut self.engine.io_set.error, "{}", err).unwrap();
                }
                Ok(Some(code)) => match self.engine.eval(code) {
                    Ok(c) => {
                        ok = true;
                        result = Some(c);
                    }
                    Err(err) => {
                        writeln!(&mut self.engine.io_set.error, "RuntimeError: {}", err).unwrap();
                        for s in self.engine.get_stack_trace() {
                            writeln!(&mut self.engine.io_set.error, "{}", s).unwrap();
                        }
                    }
                },
            },
        }

        MyLispResult {
            ok,
            result: result.map(|c| c.display(Case::Lower, Syntax::Shorten).to_string()),
        }
    }
}

// clippyの勧めにより実装…
impl Default for MyLispEngine {
    fn default() -> Self {
        Self::new()
    }
}

#[wasm_bindgen]
impl MyLispResult {
    pub fn take_result(&mut self) -> Option<String> {
        self.result.take()
    }
}

impl Prompt {
    fn fill(&mut self) {
        if self.offset < self.buf.len() {
            return;
        }
        self.offset = 0;
        self.buf.clear();
        if let Some(s) = prompt().as_string() {
            self.buf.extend_from_slice(s.as_bytes());
        }
        self.buf.push(b'\n');
    }
}

impl io::Read for Prompt {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        self.fill();
        let len: usize = buf.len().min(self.buf.len() - self.offset);
        buf[..len].copy_from_slice(&self.buf[self.offset..self.offset + len]);
        self.offset += len;
        Ok(len)
    }
}

impl io::BufRead for Prompt {
    fn consume(&mut self, mut amt: usize) {
        while amt > 0 {
            self.fill();
            let len = amt.min(self.buf.len() - self.offset);
            self.offset += len;
            amt -= len;
        }
    }

    fn fill_buf(&mut self) -> io::Result<&[u8]> {
        self.fill();
        Ok(&self.buf[self.offset..])
    }
}

impl io::Write for Output {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        // リアルタイム更新はできないぽい？…orz
        // JavaScriptの実行中に表示の反映はされないらしい？(シングルスレッドだから？)
        match String::from_utf8(buf.to_vec()) {
            Err(error) => Err(io::Error::new(io::ErrorKind::Other, error)),
            Ok(s) => {
                (self.f)(&s);
                Ok(buf.len())
            }
        }
    }

    fn flush(&mut self) -> io::Result<()> {
        Ok(())
    }
}
